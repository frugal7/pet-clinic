package com.example.petclinic.services;

import com.example.petclinic.model.Pet;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public interface PetService extends CrudService<Pet, Long> {}
