package com.example.petclinic.Controllers;

import com.example.petclinic.services.VetService;
import com.example.petclinic.services.map.VetServiceMap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class VetController {

  VetService vetService;

  VetController(VetServiceMap vetService) {
    this.vetService = vetService;
  }

  @RequestMapping({"/vets", "/vets/index", "/vets/index.html"})
  public String listVets(Model model) {
    model.addAttribute("vets", vetService.findAll());

    return "vets/index";
  }
}
