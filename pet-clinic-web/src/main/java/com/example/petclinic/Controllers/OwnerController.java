package com.example.petclinic.Controllers;

import com.example.petclinic.model.Owner;
import com.example.petclinic.services.OwnerService;
import com.example.petclinic.services.map.OwnerServiceMap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Set;

@RequestMapping("/owners")
@Controller
public class OwnerController {

  private final OwnerService ownerService;

  public OwnerController(OwnerServiceMap ownerService) {
    this.ownerService = ownerService;
  }

  @RequestMapping({"", "/index", "/index.html"})
  public String listOfOwners(Model model) {
    System.out.println((ownerService.findAll().toString()));
    model.addAttribute("owners", ownerService.findAll());


    return "owners/index";
  }
}
