package com.example.petclinic.bootstrap;

import com.example.petclinic.model.Owner;
import com.example.petclinic.model.Vet;
import com.example.petclinic.services.OwnerService;
import com.example.petclinic.services.VetService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements CommandLineRunner {
  private final OwnerService ownerService;
  private final VetService vetService;

  public DataLoader(OwnerService ownerService, VetService vetService) {
    this.ownerService = ownerService;
    this.vetService = vetService;
  }

  @Override
  public void run(String... args) throws Exception {

    Owner owner1 = new Owner();
    owner1.setFirstName("Michael");
    owner1.setLastName("Weston");
    ownerService.save(owner1);

    Owner owner2 = new Owner();
    owner2.setFirstName("Mack");
    owner2.setLastName("Johnson");
    ownerService.save(owner2);

    Vet vet = new Vet();
    vet.setFirstName("Mike");
    vet.setLastName("Wilson");
    Vet vet2 = new Vet();
    vet2.setFirstName("John");
    vet2.setLastName("West");

    vetService.save(vet);
    vetService.save(vet2);

    System.out.println("Owners and vets are loaded!");
  }
}
